# RM: EXTERNAL:
Simple Thunderbird extension that matches and replaces subjects with the following JavaScript:

~~~JS
rmExternal : function (externalStr) {
  str = externalStr;
  var extRe = /^[\s]*(RE:[\s]*)EXTERNAL:[\s]*/i;

  str = str.replace(extRe, '$1');

  return str;
  }
~~~
