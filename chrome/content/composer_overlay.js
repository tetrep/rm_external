//var Application = Components.classes["@mozilla.org/steel/application;1"].getService(Ci.steelIApplication);
//var logger = Application.console.log;
window.addEventListener("load",
    function (e) {
      rm_external.main();
  }, false);

var rm_external = {
  observe : function (subject, topic, data) {
    var msgSubjectOld = subject.gMsgCompose.compFields.subject;
    var msgSubjectNew = this.rmExternal(msgSubjectOld);
    subject.gMsgCompose.compFields.subject = msgSubjectNew;
    this.log('old message subject: ' + msgSubjectOld);
    this.log('new message subject: ' + msgSubjectNew);
  },
  register : function () {
    //observerService.notifyObservers(window, "mail:composeOnSend", null);
    var observerService = Components.classes["@mozilla.org/observer-service;1"].getService(Components.interfaces.nsIObserverService);
    observerService.addObserver(this, "mail:composeOnSend", null);
    this.log("registered");
  },
  unregister : function () {
    var observerService = Components.classes["@mozilla.org/observer-service;1"].getService(Components.interfaces.nsIObserverService);
    observerService.removeObserver(this, "mail:composeOnSend");
    this.log("unregistered");
  },
  log : function (msg) {
    //Application.console.log('rm_external: ' + msg);
  },
  rmExternal : function (instr) {
    var str = instr;
    var extRe = /^[\s]*(((RE)|(FWD)):[\s]*)EXTERNAL:[\s]*/i;

    str = str.replace(extRe, '$1');
    str = this.trimRe(str);

    this.log('before rm: ' + instr);
    this.log('after rm:  ' + str);
    return str;
  },
  trimRe : function (instr) {
    var str = instr;
    var reRe = /^[\s]*(RE:[\s]*)RE:[\s]*/i;

    str = str.replace(reRe, '$1');

    this.log('before trim: ' + instr);
    this.log('after trim:  ' + str);
    return str;
  },
  main : function () {
    this.register();
  }
};
